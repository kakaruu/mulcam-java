package day1104;

import java.util.Vector;

/*
 * [프로그램 출력결과]
   kim   56  78   12  146
   hong   46  100  97  243
   park   96  56   88  240   
 */

class Sawon {
	String name;
	int one;
	int two;
	int three;
	
	public Sawon(String name, int one, int two, int three) {
		this.name = name;
		this.one = one;
		this.two = two;
		this.three = three;
	}
	
	public int getSum() {
		return one + two + three;
	}
	
	@Override
	public String toString() {
		return name + " " + one + " " + two + " " + three + " " + getSum();
	}
}

public class Java151_Vector {
	public static void main(String[] args) {
		Vector<Sawon> v = new Vector<Sawon>();
		
		v.add(new Sawon("kim", 56, 78, 12));
		v.add(new Sawon("hong", 46, 100, 97));
		v.add(new Sawon("park", 96, 56, 88));
		
		for (Sawon sawon : v) {
			System.out.println(sawon);
		}
	}
}
