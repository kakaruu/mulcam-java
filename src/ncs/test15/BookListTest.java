package ncs.test15;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookListTest {

	public static void main(String[] args) {
		BookListTest test5 = new BookListTest();
		ArrayList<Book> list = new ArrayList<Book>();
		test5.storeList(list);// Book 객체를 3개 생성하여 리스트에 넣는다.
		test5.saveFile(list); // books.dat 파일에 리스트에 저장된 Book 객체들을 저장한다.
		List<Book> booksList = test5.loadFile();
		// books.dat 파일로부터 객체들을 읽어서 리스트에 담는다.
		test5.printList(booksList); // 리스트에 저장된 객체 정보를 출력한다.
		// 할인된 가격은 booksList 에 기록된 객체 정보를 사용하여 getter 로 계산 출력한다.
		// – for each 문을 이용 할 것

	}

//	+storeList(list:List<Book>):
//		void
//		+saveFile(list:List<Book>):
//		void
//		+loadFile():List<Book>
//		+printList(list:List<Book>):
//		void

	public void storeList(List<Book> list) {
		list.add(new Book("자바의 정석", "남궁성", 30000, "도우출판", 0.15));
		list.add(new Book("열혈강의 자바", "구정은", 29000, "프리렉", 0.2));
		list.add(new Book("객체지향 JAVA8", "금영욱", 30000, "북스홈", 0.1));
	}

	public void saveFile(List<Book> list) {
		File file = new File(".\\bin\\ncs\\test15\\books.dat");

		try (FileWriter writer = new FileWriter(file)) {
			for (Book book : list) {
				writer.append(book.getTitle() + "\t" + book.getAuthor() + "\t" + book.getPrice() + "\t"
						+ book.getPublisher() + "\t" + book.getDiscountRate() + "\n");
			}
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Book> loadFile() {
		List<Book> result = new ArrayList<Book>();

		File file = new File(".\\bin\\ncs\\test15\\books.dat");

		try (Scanner scanner = new Scanner(file)) {
			String line;
			while (scanner.hasNextLine()) {
				line = scanner.nextLine();
				if (!line.isEmpty()) {
					String[] parts = line.split("\t");
					result.add(new Book(parts[0], parts[1], Integer.parseInt(parts[2]), parts[3],
							Double.parseDouble(parts[4])));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public void printList(List<Book> list) {
		for (Book book : list) {
			System.out.println(book);
			int salePrice = (int) (book.getPrice() * (1 - book.getDiscountRate()));
			System.out.printf("할인된 가격 : %d원\n", salePrice);
		}
	}
}
