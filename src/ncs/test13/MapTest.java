package ncs.test13;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapTest {

	public static void main(String[] args) {
		// Generics 적용된 맵 객체를 선언 할당한다.
		Map<String, Inventory> map = new HashMap<String, Inventory>();

		// 상품명을 키로 사용하여 저장 처리 한다.
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy. MM. dd");
		Inventory item;
		try {
			item = new Inventory("삼성 갤럭시S7", dateFormat.parse("2016. 03. 15"), 30);
			map.put(item.getProductName(), item);

			item = new Inventory("LG G5", dateFormat.parse("2016. 02. 25"), 20);
			map.put(item.getProductName(), item);

			item = new Inventory("애플 아이패드 Pro", dateFormat.parse("2016. 01. 23"), 15);
			map.put(item.getProductName(), item);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// 맵에 기록된 정보를 연속 출력한다. EntrySet() 사용한다.
		for (Entry<String, Inventory> entry : map.entrySet()) {
			System.out.println(entry.getValue().toString());
		}
		System.out.println();

		// 맵에 기록된 정보를 Inventory[] 로 변환한 다음
		// 출고 날짜를 오늘 날짜로, 출고 수량은 모두 10개로 지정한다.예외처리함
		Inventory[] arr = new Inventory[map.size()];
		map.values().toArray(arr);
		
		for (Inventory inventory : arr) {
			try {
				inventory.setGetAmount(10);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// 변경된 Inventory[] 의 정보를 모두 출력한다.
		System.out.println("출고 수량 10 적용시 ----------------------------------------------------------");
		for (Inventory inventory : arr) {
			System.out.println(inventory.toString());
		}
	}

}
