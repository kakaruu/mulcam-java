package ncs.test10;

public class Sales extends Employee implements Bonus {
	private static final double TAX_RATE = 0.13;
	private static final double INCENTIVE_RATE = 1.2;

	public Sales() {
		super();
	}

	public Sales(String name, int number, String department, int salary) {
		super(name, number, department, salary);
	}

	@Override
	public void incentive(int pay) {
		int salary = getSalary();
		setSalary(salary + (int) (pay * INCENTIVE_RATE));
	}

	@Override
	public double tax() {
		return getSalary() * TAX_RATE;
	}
}
