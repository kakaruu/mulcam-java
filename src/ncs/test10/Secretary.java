package ncs.test10;

public class Secretary extends Employee implements Bonus {
	private static final double TAX_RATE = 0.1;
	private static final double INCENTIVE_RATE = 0.8;

	public Secretary() {
		super();
	}

	public Secretary(String name, int number, String department, int salary) {
		super(name, number, department, salary);
	}

	@Override
	public void incentive(int pay) {
		int salary = getSalary();
		setSalary(salary + (int) (pay * INCENTIVE_RATE));
	}

	@Override
	public double tax() {
		return getSalary() * TAX_RATE;
	}
}
