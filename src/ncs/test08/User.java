package ncs.test08;

public class User implements Cloneable {
	private String id;
	private String password;
	private String name;
	private int age;
	private char gender;
	private String phone;

	public User() {
	}

	public User(String id, String password, String name, int age, char gender, String phone) {
		this.id = id;
		this.password = password;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.phone = phone;
	}
	
	@Override
	public String toString() {
		// user01 pass01 김철수 32 M 010-1234-5678
		return id + " " + password + " " + name + " " + age + " " + gender + " " + phone;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof User) {
			User o = (User)obj;
			return (id == o.id || (id != null && id.equals(o.id)))
					&& (password == o.password || (password != null && password.equals(o.password)))
					&& (name == o.name || (name != null && name.equals(o.name)))
					&& (phone == o.phone || (phone != null && phone.equals(o.phone)))
					&& age == o.age
					&& gender == o.gender;
		}
		
		return false;
	}
	
	@Override
	protected User clone() throws CloneNotSupportedException {
		return (User)super.clone();
	}

	public String getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public char getGender() {
		return gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
