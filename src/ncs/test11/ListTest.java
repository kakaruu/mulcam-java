package ncs.test11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListTest {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();

		// 명시한 사용 데이터를 list 에 기록한다.
		list.addAll(Arrays.asList(new Integer[] { 69, 29, 41, 11, 2, 77, 15, 84, 98, 3 }));

		// list 의 데이터를 내림차순 정렬한다.
		list.sort(new Decending());

		// display() 메소드를 호출한다.
		display(list);
	}

	public static void display(List<?> list) {
		for (Object v : list) {
			System.out.print(v + " ");
		}
	}
}
