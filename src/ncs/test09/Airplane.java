package ncs.test09;

public class Airplane extends Plane {
	private static final int EFFICIENCY = 3;
	
	public Airplane() {
		super();
	}
	
	public Airplane(String planeName, int fuelSize) {
		super(planeName, fuelSize);
	}

	public void flight(int distance) {
		int fuel = getFuelSize();
		int needFuel = distance * EFFICIENCY;
		
		if(fuel >= needFuel) {
			setFuelSize(fuel - needFuel);
		}
	};
}
