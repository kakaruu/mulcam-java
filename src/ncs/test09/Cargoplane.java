package ncs.test09;

public class Cargoplane extends Plane {
	private static final int EFFICIENCY = 5;
	
	public Cargoplane() {
		super();
	}
	
	public Cargoplane(String planeName, int fuelSize) {
		super(planeName, fuelSize);
	}

	public void flight(int distance) {
		int fuel = getFuelSize();
		int needFuel = distance * EFFICIENCY;
		
		if(fuel >= needFuel) {
			setFuelSize(fuel - needFuel);
		}
	};
}
