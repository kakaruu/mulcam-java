package ncs.test14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GoodsTest {

	public static void main(String[] args) {
//		다음 항목의 값을 입력하시오.
//		상품명 : 인텔코어 i6
//		가격 : 229500
//		수량 : 3 

		try (BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println("다음 항목의 값을 입력하시오.");

			System.out.print("상품명 : ");
			String productName = inputReader.readLine();

			System.out.print("가격 : ");
			int price = Integer.parseInt(inputReader.readLine());

			System.out.print("수량 : ");
			int qty = Integer.parseInt(inputReader.readLine());

			Goods goods = new Goods(productName, price, qty);

			System.out.println();
			System.out.println("입력된 결과는 다음과 같습니다.");
			System.out.println(goods.toString());
			System.out.println("총 구매 가격 : " + goods.getPrice() * goods.getQuantity() + " 원");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
