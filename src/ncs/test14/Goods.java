package ncs.test14;

public class Goods {
//	name:String
//	- price:int
//	- quantity:int

	private String name;
	private int price;
	private int quantity;

	public Goods() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Goods(String name, int price, int quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		// 인텔코어 i6, 229500 원, 3 개
		return name + ", " + price + " 원, " + quantity + " 개";
	}
}
