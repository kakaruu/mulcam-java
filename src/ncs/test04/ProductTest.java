package ncs.test04;

import java.util.Scanner;

public class ProductTest {

	public static void main(String[] args) {
		String name = null;
		int price = 0;
		int qty = 0;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("상품명 입력: ");
		name = scanner.nextLine();
		System.out.print("가격 입력: ");
		price = scanner.nextInt();
		System.out.print("개수 입력: ");
		qty = scanner.nextInt();
		
		scanner.close();
		
		Product product = new Product(name, price, qty);
		
		System.out.println(product.information());
		System.out.printf("총 구매 가격 : %d 원", product.getPrice() * product.getQuantity());
	}

	
}
