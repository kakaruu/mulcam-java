package ncs.test04;

import java.text.MessageFormat;

public class Product {
	private String name;
	private int price;
	private int quantity;
	
	public Product() {
	}

	public Product(String name, int price, int quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String information() {
		return MessageFormat.format(
				"상품명 : {0}\r\n"
				+ "가격 : {1,number,#} 원\r\n"
				+ "수량 : {2,number,#} 개",
				name, price, quantity);
	}
}
