package java1105_stream.prob;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.Scanner;

/*
 * [문제]
 *  jumsu.txt 파일로부터 학생의 점수를 읽어들여 총점과 평균을 jumsu.txt 파일 끝에 덧붙여 출력하는 프로그램을 작성하시오.
    단, 평균점수는 소수점 첫 번째 자리까지만 남기고 나머지는 잘라낸다.(truncate) 
    
    [프로그램 실행결과]
    태연:65
	수영:97
	제시카:100
	티파니:86
	써니:88
	총점:436
	평균:87.2
 */
public class Prob004_stream {
	public static void main(String[] args) {
		// 프로그램을 구현하시오.
		File fr=new File(".\\src\\java1105_stream\\prob\\jumsu.txt");
		
		int count = 0;
		int sumOfScore = 0;
		try (Scanner scanner = new Scanner(fr)) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				
				int score = Integer.parseInt(line.split(":")[1]);
				sumOfScore += score;
				count++;
				
				System.out.println(line);
			}
		} catch (FileNotFoundException e) {
			System.out.println("파일을 찾을 수 없습니다.");
			e.printStackTrace();
		}
		System.out.println("총점:" + sumOfScore);
		System.out.println("평균:" + MessageFormat.format("{0,number,#.#}", ((double)sumOfScore / count)));
		
	}//end main()
}//end class








