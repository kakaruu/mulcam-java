package java1105_stream.prob;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * [문제]
 * phone.txt파일에 저장된 값들을  phoneProduct()메서드에서
 * SmartPhone에 저장한 후 ArrayList에 추가한후 리턴하고,
 * phoneList()메소드에서 프로그램 실행결과처럼 출력하는 프로그램을
 * 구현하시오.
 * 
 * [프로그램 실행결과]
 * << 1 번째 상품 >>
	제품 아이디 : PROD-00001
	제품명 : 아이폰5
	가격 : 940000
	수량 : 4
	제조사 : 애플
   << 2 번째 상품 >>
	제품 아이디 : PROD-00002
	제품명 : 갤럭시S
	가격 : 860000
	수량 : 3
	제조사 : 삼성전자

 */
public class Prob007_ArrayList {

	public static void main(String[] args) {
		String pathFile=".\\src\\java1105_stream\\prob\\phone.txt";
		ArrayList<SmartPhone> phoneList = phoneProduct(pathFile);
		prnDisplay(phoneList);
	}//end main()
	
	private static ArrayList<SmartPhone> phoneProduct(String pathFile) {
		// phone.txt파일의 데이터를 ArrayList에 저장한후 리턴하는 프로그램을 구현하시오.
		ArrayList<SmartPhone> result = new ArrayList<SmartPhone>();
		
		File file = new File(pathFile);

		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] parts = line.split(":");
				
				SmartPhone phone = new SmartPhone();
				phone.setProductId(parts[0]);
				phone.setName(parts[1]);
				phone.setPrice(Integer.parseInt(parts[2]));
				phone.setAmount(Integer.parseInt(parts[3]));
				phone.setMaker(parts[4]);
				
				result.add(phone);
			}
		} catch (FileNotFoundException e) {
			System.out.println("파일을 찾을 수 없습니다.");
			e.printStackTrace();
		}
		
		return result;
	}//end phoneProduct( )
	
	private static void prnDisplay(ArrayList<SmartPhone> phoneList){
		//phoneList매개변수의 저장된 값을 출력하는 프로그램을 구현하시오.
//		   << 1 번째 상품 >>
//			제품 아이디 : PROD-00001
//			제품명 : 아이폰5
//			가격 : 940000
//			수량 : 4
//			제조사 : 애플
//		   << 2 번째 상품 >>
//			제품 아이디 : PROD-00002
//			제품명 : 갤럭시S
//			가격 : 860000
//			수량 : 3
//			제조사 : 삼성전자
		int num = 1;
		for (SmartPhone smartPhone : phoneList) {
			System.out.printf("<< %d 번째 상품 >>\n", num++);
			System.out.println("제품 아이디 : " + smartPhone.getProductId());
			System.out.println("제품명 : " + smartPhone.getName());
			System.out.println("가격 : " + smartPhone.getPrice());
			System.out.println("수량 : " + smartPhone.getAmount());
			System.out.println("제조사 : " + smartPhone.getMaker());
		}

	}//end prnDisplay( )

}//end class











