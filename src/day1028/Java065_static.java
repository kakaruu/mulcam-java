package day1028;

public class Java065_static {
	public static void main(String[] args) {
		System.out.println("static main");
		
		System.out.println(OrderStatic.y);
		new OrderStatic();
	}

}

class OrderStatic {
	static int y = 0;
	
	static {
		// 클래스를 처음 참조할 때 실행 됨
		System.out.println("클래스 내부 static");
	}
	
//	java에서는 static 생성자를 지원하지 않는다.
//	static OrderStatic() {
//		System.out.println("static 생성자");
//	}
	
	OrderStatic() {
		System.out.println("일반 생성자");
	}
}
