package java1108_thread.prob.part01;

public class Consumer extends Thread {
	private VendingMachine machine;

	public Consumer(VendingMachine machine) {
		this.machine = machine;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 10; i++) {
			String drink = machine.getDrink();
		}
	}
}