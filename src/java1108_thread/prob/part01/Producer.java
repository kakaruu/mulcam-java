package java1108_thread.prob.part01;

public class Producer extends Thread {
	private VendingMachine machine;

	public Producer(VendingMachine machine) {
		this.machine = machine;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 10; i++) {
			String drink = "음료수 No. " + i;
			machine.putDrink(drink);
		}
	}
}
