package java1108_thread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Java200_ChatServer {

	public static void main(String[] args) {
		ServerSocket server = null;
		Socket client = null;

		try {
			server = new ServerSocket(7777);
			while(true) {
				client = server.accept();
				
				System.out.printf("client가 %s로 접속\n", client.getInetAddress().getHostName());
				
				Java200_ChatHandler handler = new Java200_ChatHandler(client);
				handler.initStart();				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
