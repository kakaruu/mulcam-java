package al211119;

public class UsedMemory {

	public static void main(String[] args) {
		// 공간 복잡도(메모리 사용량) 구하는 방법
		
		// gc로 메모리 정리
		System.gc();
		
		// 실행 전 메모리 사용량
		long before = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

		int j = 0;
		for(int i = 0; i < 100000000; i++ ) {
			j = j + i;
		}
		
		// gc로 메모리 정리
		System.gc();

		// 실행 후 메모리 사용량
		long after = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		
		// 왜 after를 빼는지 모르겠는데, 아마 함수 실행시 필요한 메모리를 미리 할당하나보다.
		long usedMemory = before - after;
		
		System.out.println("메모리 사용량 : " + usedMemory);
	}

}
