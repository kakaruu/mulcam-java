package day1026.prob.prob03;
/*
 * 4행 4열 data배열에 가로 세로 합계를 구하는 프로그램을 구현하시오.
 * [출력결과]
 *  1   2   3   6
 *  4   5   6  15
 *  7   8   9  24
 * 12  15  18  45
 */

public class Prob_02 {

	public static void main(String[] args) {
		int[][] data = new int[4][4];
		int cnt = 1;
		
		// 여기를 구현하시오.
		
		// 배열 초기화
		int rowSize = data.length;
		for(int row = 0; row < rowSize - 1; row++) {
			int colSize = data[row].length;
			for(int col = 0; col < colSize - 1; col++) {
				// 0 ~ size - 2 까지는 cnt를 삽입
				data[row][col] = cnt++;
				
				// size - 1(마지막 항목)에는 누산
				data[row][colSize - 1] += data[row][col];
				data[rowSize - 1][col] += data[row][col];
//				data[rowSize - 1][colSize - 1] += data[row][col];
			}
			data[rowSize - 1][colSize - 1] += data[row][colSize - 1];
		}
		
		for(int row = 0; row < data.length; row++) {
			for(int col = 0; col < data[row].length; col++) {
				System.out.printf("%4d", data[row][col]);
			}
			System.out.println();
		}
	}// end main()

}// end class
