package day1026;

public class Java036_array {

	public static void main(String[] args) {
		int[] nums = new int[] { 22, 3, 8 ,12 };
		int sumEven = 0;
		int sumOdd = 0;
		
		for(int i = 0; i < nums.length; i++) {
			if(nums[i] % 2 == 0) {
				sumEven += nums[i];
			} else {
				sumOdd += nums[i];
			}
		}
		
		System.out.println("홀수합=" + sumOdd);
		System.out.println("짝수합=" + sumEven);
	}

}
