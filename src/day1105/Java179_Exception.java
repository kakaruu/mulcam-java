package day1105;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Java179_Exception {

	public static void main(String[] args) {
		// Closeable을 상속하고 있는 클래스들은 이렇게 사용하면
		// try문이 끝날 때 자동으로 close 메소드를 호출해준다.
		try (FileReader fr1 = new FileReader("sadf");
				FileReader fr2 = new FileReader("sadf")) {

		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
		}
	}

}
