package day1103.prob;

import java.util.Random;
import java.util.Scanner;
/*
	1부터 100까지 숫자를 입력하세요:50
	1과 100사이의 값 중 입력한 값보다 큰 값을 입력하세요.
	1부터 100까지 숫자를 입력하세요:90
	1과 100사이의 값 중 입력한 값보다 작은 값을 입력하세요.
	1부터 100까지 숫자를 입력하세요:85
	1과 100사이의 값 중 입력한 값보다 큰 값을 입력하세요.
	1부터 100까지 숫자를 입력하세요:88
	사용자가 입력한 값과 컴퓨터가 발생시킨 임의의 값이 일치합니다.
	총 실행 횟수는 4번 입니다.
 */

public class Prob001_Scanner {
	public static void main(String[] args) {
		Scanner key = new Scanner(System.in);
		/* randomNum에 1부터 100까지의 숫자 중 컴퓨터가 임의로 발생시킨 값이 저장되도록 작성합니다.*/
		int randomNum =0 ;//컴퓨터가 임의로 발생시키는 값을 저장할 변수
		int userNum =0;//사용자가 입력할 값을 저장할 변수
		int count = 0;//실행횟수를 저장할 변수
		//randomNum과 userNum이 일치할때 까지 반복하여 입력받아 평가하는 코드를 작성하세요.
		
		boolean isUpper = false;
		Random rand = new Random();
		userNum = 100;
		while(true) {
			if(count > 0) {
				if(isUpper){
					System.out.println("1과 100사이의 값 중 입력한 값보다 큰 값을 입력하세요.");
				} else {
					System.out.println("1과 100사이의 값 중 입력한 값보다 작은 값을 입력하세요.");
				}
			}
			
			// 1 이상, 100 미만
			randomNum = rand.nextInt(99) + 1;
			
			System.out.print("1부터 100까지 숫자를 입력하세요:");
			String input = key.nextLine();
			try {
				int inputNum = Integer.parseInt(input);
				
				if(inputNum > 0 && inputNum < 100
					&& (isUpper && inputNum > userNum
						|| !isUpper && inputNum < userNum)){
					userNum = inputNum;
					count++;
				} else {
					System.out.println("조건에 맞는 값을 입력해주세요.");
					continue;
				}
			} catch (Exception e) {
				System.out.println("숫자값을 입력해주세요.");
				continue;
			}
			
			if(userNum == randomNum) {
				System.out.println("사용자가 입력한 값과 컴퓨터가 발생시킨 임의의 값이 일치합니다.");
				System.out.printf("총 실행 횟수는 %d번 입니다.", count);
				break;
			}
			isUpper = !isUpper;
		}
		key.close();
		
		
	}//end main()

}//end class










