package day1103.prob;

import java.util.Arrays;

/*
 * javac Prob001_String.java
 * java Prob001_String JAva Test
 */
/*1 프로그램 실행할때  "JAva Test" 문자열을 args배열에서 값을 받아 출력결과에 같이
 *  나오도록 프로그램을 구현하시오.
 *  java Prob001_String JAva Test
 *   * 
 * 2 출력결과
 *   source : JAva Test 
 *   convert: jaVA tEST
 *   length: 9
 *   reverse : tseT avAJ
 */

public class Prob002_String {

	public static void main(String[] args) {
		System.out.println(args[0]);
		System.out.println(args[1]);
		
		String source = Arrays
	            .stream(args)
	            .reduce((a, b) -> a + " " + b)
	            .get();
		
		StringBuilder convert = new StringBuilder();
		for (char c : source.toCharArray()) {
			convert.append(Character.isUpperCase(c) ? 
	                Character.toLowerCase(c) : 
	                Character.toUpperCase(c));
		}
		
		StringBuilder reverse = new StringBuilder(source);
		reverse.reverse();
		
		System.out.println("source: " + source);
		System.out.println("convert: " + convert);
		System.out.println("length: " + source.length());
		System.out.println("reverse: " + reverse);
	}
		
}// end class

