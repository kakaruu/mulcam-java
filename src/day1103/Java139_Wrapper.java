package day1103;

import java.util.Scanner;

/*
 * 이름, 학점을 입력하는 프로그램을 구현하시오.
 * 입력 : 홍길동,80,93
 * 
 * [출력결과]
 * 이름: 홍길동
 * 국어: 80
 * 영어: 93
 * 평균: 86.5 (double로 계산)
 */
public class Java139_Wrapper {

	public static void main(String[] args) {
		String pattern = "(\\p{L}*),(\\d|[1-9]\\d|100),(\\d|[1-9]\\d|100)";
		
		String input = "";
		Scanner scanner = new Scanner(System.in);
		do {
			input = scanner.nextLine();
			if(!input.matches(pattern)) {
				System.out.println("'이름,국어점수,영어점수' 형식으로 입력해주세요.");
			} else {
				break;
			}
		} while(true);
		scanner.close();

		System.out.println(input);
	}
}
