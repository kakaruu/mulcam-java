package day1101;

//다중 상속 시 메소드 이름이 겹치고 반환값이 다를 경우 충돌이 발생할 수 있다.
//public class Test implements ITest1, ITest2 {
//	@Override
//	public int getSome() {
//		return 0;
//	}
//
//	@Override
//	public String getSome() {
//		return 0;
//	}
//}

// 솔루션1.. 이러면 근데 인터페이스 쓰는 이유가..
public class Test {
	ITest1 test1;
	ITest2 test2;
}

// 솔루션2
//public class Test implements ITest1 {
//	@Override
//	public int getSome() {
//		return 0;
//	}
//	
//	public String getSome2() {
//		return new ITest2() {
//			
//			@Override
//			public String getSome() {
//				return null;
//			}
//		}.getSome();
//	}
//}