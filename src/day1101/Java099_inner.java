package day1101;

class Outer {
	private int num = 10;
	
	class Inner {
		// Outer의 인스턴스가 생성되었을 때만 Inner에 접근할 수 있기 때문에
		// 이런식으로 사용이 가능하다.
		private int num = Outer.this.num;
		public void prn() {
			num = 20;
			System.out.println(Outer.this.num);
			System.out.println(num);
		}
	}
}

public class Java099_inner {
	
	public static void main(String[] args) {
		Outer outer = new Outer();
		Outer.Inner inner = outer.new Inner();
		
		inner.prn();
	}

}
