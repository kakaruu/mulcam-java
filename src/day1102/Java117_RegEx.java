package day1102;

/*
 * 아이디를 입력할때 영문자,숫자 조합으로 합니다. 
 *   최소 5자이상 10자이하까지만 가능합니다.
 * (반드시 영문자로 시작한다.)
 * [출력결과]
 *  로그인 되었습니다.
 *  회원이 아닙니다.
 */
public class Java117_RegEx {

	public static void main(String[] args) {
		System.out.print("ymy232");
		display(process("ymy232"));		

		System.out.print("korea");
		display(process("korea"));

	}//end main()

	public static boolean process(String sn) {
		// 여기를 구현하세요.
		
		// 반드시 영문자, 숫자가 한번씩 나오도록
//		return sn.matches("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z][A-Za-z\\d]{4,9}$");
		
		// 영문자 숫자가 한번씩 나오지 않아도 되는 경우
		return sn.matches("^[A-Za-z][A-Za-z\\d]{4,9}$");
	}//end process()
	
	public static void display(boolean res) {
		System.out.println(" " + (res ? "로그인 되었습니다." : "회원이 아닙니다."));
	}//end display()
	
}//end class
