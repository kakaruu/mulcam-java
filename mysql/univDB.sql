# 2021-11-17

CREATE DATABASE univDB;

USE univDB;

CREATE TABLE 학생(
 학번 CHAR(4) NOT NULL,
이름 VARCHAR(20) NOT NULL,
주소 VARCHAR(50) NULL DEFAULT '미정',
학년 INT NOT NULL,
나이 INT NULL,
성별 CHAR(2) NOT NULL,                             
휴대폰번호 CHAR(14) null, 
소속학과 VARCHAR(20) NULL,
PRIMARY KEY(학번)  );


CREATE TABLE 과목
 ( 과목번호 char(4) NOT NULL PRIMARY KEY,
   이름 VARCHAR(20) NOT NULL,
   강의실 CHAR(3) NOT NULL,
   개설학과 VARCHAR(20) NOT NULL,
   시수 INT NOT NULL );

CREATE TABLE 수강
 ( 학번 char(6) NOT NULL,
   과목번호 CHAR(4) NOT NULL,
   신청날짜 DATE NOT NULL,
   중간성적 INT NULL DEFAULT 0,
   기말성적 INT NULL DEfaULT 0,
   평가학점 CHAR(1) NULL,       
   PRIMARY KEY(학번, 과목번호));


 INSERT INTO 학생
 VALUES('s001','김연아','서울 서초', 4, 23, '여', '010-1111-2222', '컴퓨터');
 INSERT INTO 학생
 VALUES('s002','홍길동',DEFAULT, 1, 26, '남', NULL, '통계');
 INSERT INTO 학생
 VALUES('s003','이승엽',NULL, 3, 30, '남', NULL, '정보통신');
 INSERT INTO 학생
 VALUES('s004','이영애','경기 분당', 2, NULL, '여', '010-4444-5555', '정보통신');
 INSERT INTO 학생
 VALUES('s005','송윤아','경기 분당', 4, 23, '여', '010-6666-7777', '컴퓨터');
 INSERT INTO 학생
 VALUES('s006','홍길동','서울 종로', 2, 26, '남', '010-8888-9999', '컴퓨터');
 INSERT INTO 학생
 VALUES('s007','이은진','경기 과천', 1, 23, '여', '010-2222-3333', '경영');

INSERT INTO 과목
VALUES ('c001', '데이터베이스', 126, '컴퓨터', 3) ;
INSERT INTO 과목
VALUES ('c002', '정보보호', 137, '정보통신', 3) ;
INSERT INTO 과목
VALUES ('c003', '모바일웹', 128, '컴퓨터', 3) ;
INSERT INTO 과목
VALUES ('c004', '철학개론', 117, '철학', 2) ;
INSERT INTO 과목
VALUES ('c005', '전공글쓰기', 120, '교양학부', 1) ;

INSERT INTO 수강
VALUES ('s001', 'c002', '2019-09-03', 93, 98, 'A') ;
INSERT INTO 수강
VALUES ('s004', 'c005', '2019-03-03', 72, 78, 'C') ;
INSERT INTO 수강
VALUES ('s003', 'c002', '2017-09-06', 85, 82, 'B') ;
INSERT INTO 수강
VALUES ('s002', 'c001', '2018-03-10', 31, 50, 'F') ;
INSERT INTO 수강
VALUES ('s001', 'c004', '2019-03-05', 82, 89, 'B') ;
INSERT INTO 수강
VALUES ('s004', 'c003', '2020-09-03', 91, 94, 'A') ;
INSERT INTO 수강
VALUES ('s001', 'c005', '2020-09-03', 74, 79, 'C') ;
INSERT INTO 수강
VALUES ('s003', 'c001', '2019-03-03', 81, 82, 'B') ;
INSERT INTO 수강
VALUES ('s004', 'c002', '2018-03-05', 92, 95, 'A') ;

-- 1. 전체 학생의 이름과 주소를 검색하시오.
SELECT 이름, 주소 FROM 학생;

-- 2. 전체 학생의 모든 정보를 검색하시오.
SELECT * FROM 학생;

-- 3. 전체 학생의 소속학과 정보를 중복 없이 검색하시오.
SELECT 소속학과 FROM 학생 GROUP BY 소속학과;

-- 4. 학생 중에서 2학년 이상인 '컴퓨터'학과 학생의 이름, 학년, 소속학과, 휴대폰번호 정보를 검색하시오.
SELECT 이름, 학년, 소속학과, 휴대폰번호 FROM 학생 WHERE 학년 >= 2 AND 소속학과 = '컴퓨터';

-- 5. 1,2,3학년 학생이거나 '컴퓨터'학과에 소속되지 않은 학생의 이름, 학년, 소속학과, 휴대폰번호 정보를 검색하시오. 
SELECT 이름, 학년, 소속학과, 휴대폰번호 FROM 학생 WHERE 학년 IN (1,2,3) OR 소속학과 != '컴퓨터';


-- 6. '컴퓨터'과나 '정보통신'학과의 학생의 이름과 학년, 소속학과 정보를 학년의 오름차순(학년이 낮은 학생부터 높은 학생 순서)으로 검색하시오.
SELECT 이름, 학년, 소속학과 FROM 학생 WHERE 소속학과 = '컴퓨터' OR 소속학과 = '정보통신' ORDER BY 학년;

-- 7. 전체 학생의 모든 정보를 검색하되 학년을 기준으로 먼저 1차 오름차순 정렬하고, 학년이 같은 경우에는 이름을 기준으로 
--     2차 내림차순 정렬하여 검색하시오.
SELECT
	*
FROM 학생
ORDER BY 학년, 이름 DESC;

-- 8. 전체 학생수를 검색하시오. 
SELECT
	COUNT(*) AS `전체 학생수`
FROM 학생;

-- 9. '여' 학생의 평균 나이를 검색하시오.  
SELECT
	AVG(나이) AS `평균 나이`
FROM 학생
WHERE 성별 = '여';

-- 10. 전체 학생의 성별 최고 나이와 최저 나이를 검색하시오. 
SELECT
	성별,
	MAX(나이) AS `최고 나이`,
	MIN(나이) AS `최저 나이`
FROM 학생
GROUP BY 성별;
	

-- 11. 20대 학생만을 대상으로 나이별 학생수를 검색하시오.
SELECT
	나이, COUNT(*) AS `나이별 학생수`
FROM 학생
WHERE 나이 >= 20 AND 나이 < 30
GROUP BY 나이;

-- 12. 각 학년별로 2명 이상의 학생을 갖는 학년에 대해셔만 학년별 학생수를 검색하시오.
SELECT
	학년,
	COUNT(*) AS `학년별 학생수`
FROM 학생
GROUP BY 학년
HAVING `학년별 학생수` >= 2;

-- 13. '이'씨 성을 가진 학생들의 학번과 학생 이름을 검색하시오.
SELECT
	학번, 이름
FROM 학생
WHERE 이름 LIKE '이%';

-- 14. 주소지가 '서울'인 학생의 이름, 주소, 학년을 학년 순(내림차순)으로 검색하시오.
SELECT
	이름, 주소, 학년
FROM 학생
WHERE 주소 LIKE '서울%'
ORDER BY 학년 DESC;

-- 15. 휴대폰번호가 등록되지 않은(널 값을 갖는)학생의 이름과 휴대폰번호를 검색하시오.
SELECT
	이름, 휴대폰번호
FROM 학생
WHERE 휴대폰번호 IS NULL;

-- 16. '여'학생이거나 'A'학점을 받은 학생의 학번을 검색하시오. 
SELECT DISTINCT 학생.학번
FROM 학생 LEFT JOIN 수강
ON 학생.학번 = 수강.학번
WHERE
	학생.성별 = '여'
    OR 수강.평가학점 = 'A';
    
SELECT 학번
FROM 학생
WHERE 성별 = '여'
UNION
SELECT 학번
FROM 수강
WHERE 수강.평가학점 = 'A';

-- 17_1. 과목번호가 'c002'인 과목을 수강한 학생의 이름을 검색하시오.
SELECT 학생.이름
FROM 학생, 수강
WHERE
	수강.과목번호 = 'c002'
	AND 학생.학번 = 수강.학번;


-- 17_2. '정보보호' 과목을 수강한 학생의 이름을 검색하시오.
SELECT 학생.이름
FROM 학생, 과목, 수강
WHERE
	과목.이름 = '정보보호'
    AND 과목.과목번호 = 수강.과목번호
	AND 학생.학번 = 수강.학번;


-- 17_3. 학생 중에서 한 과목도 수강하지 않은 학생의 이름을 검색하시오. 
SELECT 학생.이름
FROM 학생 LEFT JOIN 수강
ON 학생.학번 = 수강.학번
WHERE 수강.과목번호 IS NULL;

                  
 -- 18. 전체 학생의 기본 정보와 모든 수강 정보를 검색하시오. 
SELECT *
FROM 학생 LEFT JOIN 수강
ON 학생.학번 = 수강.학번;
 
 
 -- 19. 학생 중에서 과목번호가 'c002'인 과목을 수강한 학생의 학번과 이름, 과목번호 그리고 
 --     변환중간성적(학생별 중간 성적의 10% 가산점수)을 검색하시오.
SELECT
	학생.학번,
    학생.이름,
    수강.과목번호,
    수강.중간성적 * 1.1 AS 변환중간성적
FROM 학생, 수강
WHERE
	수강.과목번호 = 'c002'
	AND 학생.학번 = 수강.학번;


-- 21. 학생 중에서 과목번호가 'c002'인 과목을 수강한 학생의 이름, 과목번호를 검색하시오. 
SELECT
    학생.이름,
    수강.과목번호
FROM 학생, 수강
WHERE
	수강.과목번호 = 'c002'
	AND 학생.학번 = 수강.학번;


-- 22. 주소가 같은 학생들의 이름을 쌍으로 검색하시오. 이때, 검색되는 첫 번째 학생이  두 번째 학생보다 학년이 높도록 하시오.
SELECT
	주소,
    GROUP_CONCAT(이름 ORDER BY 학년 DESC) AS `학생들의 이름`
FROM 학생
GROUP BY 주소;

SELECT
	`학생1`.주소,
    `학생1`.이름,
    `학생2`.이름
FROM 학생 AS `학생1` JOIN 학생 AS `학생2`
ON `학생1`.주소 = `학생2`.주소
WHERE `학생1`.학년 > `학생2`.학년;

-- 23. 과목을 수강하지 않은 학생을 포함하여 모든 학생의 학번, 이름과 학생이 수강한 교과의 평가학점을 검색하시오. 
SELECT
	학생.학번,
    학생.이름,
    수강.과목번호,
    과목.이름 AS `과목이름`,
    수강.평가학점
FROM 학생 LEFT JOIN 수강
ON 학생.학번 = 수강.학번
LEFT JOIN 과목
ON 수강.과목번호 = 과목.과목번호;


# 수업 내용 시작

ALTER TABLE `수강` 
ADD CONSTRAINT `FK_학생_수강`
  FOREIGN KEY (`학번`)
  REFERENCES `학생` (`학번`);

CREATE TABLE 과목2
 ( 과목번호 char(4) NOT NULL PRIMARY KEY,
   이름 VARCHAR(20) NOT NULL,
   강의실 CHAR(3) NOT NULL,
   개설학과 VARCHAR(20) NOT NULL,
   시수 INT NOT NULL );
   
CREATE TABLE 학생2(
 학번 CHAR(4) NOT NULL,
이름 VARCHAR(20) NOT NULL,
주소 VARCHAR(50) NULL DEFAULT '미정',
학년 INT NOT NULL,
나이 INT NULL,
성별 CHAR(2) NOT NULL,                             
휴대폰번호 CHAR(14) null, 
소속학과 VARCHAR(20) NULL,
PRIMARY KEY(학번),
UNIQUE(휴대폰번호)  );

CREATE TABLE 수강2
 ( 학번 char(6) NOT NULL,
   과목번호 CHAR(4) NOT NULL,
   신청날짜 DATE NOT NULL,
   중간성적 INT NULL DEFAULT 0,
   기말성적 INT NULL DEfaULT 0,
   평가학점 CHAR(1) NULL,       
   PRIMARY KEY(학번, 과목번호),
   FOREIGN KEY(학번) REFERENCES 학생2(학번),
   FOREIGN KEY(과목번호) REFERENCES 과목2(과목번호));
   
INSERT INTO 과목2(과목번호, 이름, 강의실, 개설학과, 시수) VALUES ('c111', 'database', 'A12', '산업공학', 3);
# NOT NULL로 되어있는 컬럼에 값을 안 넣을 시 에러 발생.. 당연한걸 굳이 시연까지..
# INSERT INTO 과목2(과목번호, 이름, 강의실, 개설학과) VALUES ('c112', 'database', 'A12', '산업공학');

INSERT INTO 학생2
(학번, 이름, 학년, 나이, 성별, 휴대폰번호, 소속학과)
VALUES('s111','김연아', 4, 23, '여', NULL, '체육학과');

INSERT INTO 학생2
(학번, 이름, 학년, 나이, 성별, 휴대폰번호, 소속학과)
VALUES('s112','이상아', 3, 45, '여', '010-1111-1111', '연극학과');

# 중복값 테스트.. 이것도 굳이..?
-- INSERT INTO 학생2
-- (학번, 이름, 학년, 나이, 성별, 휴대폰번호, 소속학과)
-- VALUES('s113','박상아', 2, 35, '여', '010-1111-1111', '미술학과');

INSERT INTO 수강2
(학번, 과목번호, 신청날짜, 평가학점)
VALUES ('s111', 'c111', '2019-12-31', 'A') ;

# 외래키 RESTRICT 제약 조건
-- INSERT INTO 수강2
-- (학번, 과목번호, 신청날짜, 평가학점)
-- VALUES ('s113', 'c111', '2019-12-20', 'B') ;

-- '학생2' 테이블에 새로운 '등록날짜'열을 추가하시오.
ALTER TABLE `학생2` 
ADD COLUMN `등록날짜` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
