# 2021-11-12

use employees;

# 중복 제거
SELECT DISTINCT title FROM titles;
SELECT title FROM titles GROUP BY title;

# DISTRINCT와 GROUP BY의 차이
# GROUP BY는 정렬이 포함된다. 즉, DISTRINCT + ORDER BY ASC
# 따라서 GROUP BY가 성능이 좀 더 낮을 수 있다.
# 그리고 COUNT를 사용할 때의 결과도 다르다.
SELECT COUNT(DISTINCT title) FROM titles; # title의 가짓수를 구함
SELECT title, COUNT(title) FROM titles GROUP BY title; # title로 그룹을 구분하고 그룹내의 개수를 구함

# 테이블 복사
CREATE TABLE copy_salaries (
	SELECT * FROM salaries WHERE emp_no = 10001);
SELECT * FROM copy_salaries;
DROP TABLE copy_salaries;

# 테이블의 구조만 복사
CREATE TABLE copy_salaries (
	SELECT * FROM salaries WHERE FALSE);
SELECT * FROM copy_salaries;
DROP TABLE copy_salaries;

# GROUP BY HAVING
# GROUP BY의 WHERE 절이라고 생각하면 된다.
# WHERE이랑은 서순이 다른듯하다.
SELECT count(*), gender
FROM employees
GROUP BY gender;

SELECT count(*), gender
FROM employees
WHERE emp_no > 100000
GROUP BY gender;

# HAVING 절에서는 GROUP BY에 의해 그룹핑된 컬럼만 사용할 수 있다.
# 따라서 아래와 같은 쿼리는 사용할 수 없다.
-- SELECT count(*), gender
-- FROM employees
-- GROUP BY gender
-- HAVING emp_no > 100000;

# 요렇게 그룹 후의 컬럼만 사용 가능하다.
SELECT count(*), gender
FROM employees
GROUP BY gender
HAVING count(*) > 130000;

# MySQL의 경우 GROUP BY와 HAVING 절에서 동일한 구문을 사용해도
# 구문(YEAR(birth_date))의 동일시 처리가 되지 않아서 오류가 발생하는 경우가 있다.
-- SELECT YEAR(birth_date), count(*)
-- FROM employees
-- WHERE first_name = "Mary"
-- GROUP BY YEAR(birth_date)
-- HAVING YEAR(birth_date) > 1960
-- ORDER BY count(*);

# 이럴때는 아래처럼 Alias를 사용해서 동일한 항목이라는 것을 SQL 해석기가 인식할 수 있도록 해주면 된다.
SELECT YEAR(birth_date) AS `Year`, count(*)
FROM employees
WHERE first_name = "Mary"
GROUP BY `Year`
HAVING `Year` > 1960
ORDER BY count(*);

# WITH ROLLUP문을 사용하면 마지막 ROW로 합계가 출력된다.
SELECT count(*), gender
FROM employees
GROUP BY gender
WITH ROLLUP;
