package java1108_thread.answ.part01;

public class Prob001_thread {

	public static void main(String[] args) {
		VendingMachine vm = new VendingMachine();

		Producer p = new Producer(vm);
		Thread t1 = new Thread(p, "생산자1");
		t1.start();		

		Consumer c = new Consumer(vm);
		Thread t2 = new Thread(c, "소비자1");		
		t2.start();		
		
	}// end main()

}// end class
