package java1108_thread.answ.part01;

public class Producer implements Runnable{
	private VendingMachine vm;
	
	public Producer() {
	
	}
	public Producer(VendingMachine vm) {
		this.vm=vm;
	}
	
	@Override
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.printf("%s : 음료수 No. %d 넣음\n", Thread.currentThread().getName(), i );
			vm.putDrink("음료수 No. "+i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}//end run()///////////

}//end 
