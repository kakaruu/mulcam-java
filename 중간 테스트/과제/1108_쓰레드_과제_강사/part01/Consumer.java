package java1108_thread.answ.part01;

public class Consumer implements Runnable{
	private VendingMachine vm;
	
	public Consumer() {
		
	}
	
	public Consumer(VendingMachine vm) {
		this.vm=vm;
	}
	
	
	@Override
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.printf("%s : %s 꺼내옴.\n", 
					Thread.currentThread().getName(), vm.getDrink());
		}
	}//end run()

}//end class
