package java1108_thread.prob.part01;

import java.util.LinkedList;
import java.util.Queue;

public class VendingMachine {
	Queue<String> drinks = new LinkedList<String>();

	public synchronized String getDrink() {
		while(drinks.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		String drink = drinks.poll();
		System.out.printf("%s : %s 꺼내먹음\n", Thread.currentThread().getName(), drink);
		System.out.println();
		
		notifyAll();
		
		return drink;
	}

	public synchronized void putDrink(String drink) {
		if(!drinks.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		drinks.offer(drink);
		System.out.printf("%s : %s 자판기에 넣기\n", Thread.currentThread().getName(), drink);
		
		notifyAll();
	}
}
