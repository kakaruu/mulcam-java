package java1105_stream.prob;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Pattern;

/*
 * [문제] 
 * input.txt 파일에는 팝송 가사가 들어있다. 
 * 이 파일에서 검색하고자 하는 문자열이 포함되어있는 라인의 번호와 
 * 가사를 콘솔에 출력하는 search(String inputFile, String searchWord) 
 * 메서드를 구현하시오.
 * 
 * [프로그램 실행결과]
 * 5 line : It exists to give You comfort
 * 6 line : It is there to keep you warm
 * 9 line : When You are most alone
 * 10 line : The memory of love will bring you home
 * 14 line : It invites you to come closer
 * 15 line : It wants to show you more
 * 17 line : And even if you lose yourself
 * 20 line : will see you through
 * 39 line : My memories of love will be of you
 */

public class Prob003_stream {
	public static void main(String[] args) throws Exception {

		search(".\\src\\java1105_stream\\prob\\input.txt", "You");
	}// end main()

	private static void search(String inputFile, String searchWord) {
		//여기를 구현하세요.
		try (FileReader fr = new FileReader(inputFile);
				LineNumberReader reader = new LineNumberReader(fr)) {
			Pattern pattern = Pattern.compile(searchWord, Pattern.CASE_INSENSITIVE);
			
			String line = null;
			while ((line = reader.readLine()) != null) {
				if(pattern.matcher(line).find()) {
					System.out.printf("%d line : %s\n", reader.getLineNumber(), line);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("파일을 찾을 수 없습니다.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("입출력 에러");
			e.printStackTrace();
		}
	}// end search()
}// end class
