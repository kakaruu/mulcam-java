CREATE DATABASE examDB;

USE examDB;

CREATE TABLE `examDB`.`book` (
  `bookNo` CHAR(10) NOT NULL COMMENT '도서 번호',
  `bookTitle` VARCHAR(30) NULL COMMENT '도서 제목',
  `bookAuthor` VARCHAR(20) NULL COMMENT '저자',
  `bookYear` INT NULL COMMENT '발행연도',
  `bookPrice` INT NULL COMMENT '가격',
  `bookPublisher` CHAR(10) NULL COMMENT '출판사',
  PRIMARY KEY (`bookNo`));

INSERT INTO `examDB`.`book` (`bookNo`, `bookTitle`, `bookAuthor`, `bookYear`, `bookPrice`, `bookPublisher`) VALUES ('B001', '자바프로그래밍', '홍길동', '2021', '30000', '서울출판사');
INSERT INTO `examDB`.`book` (`bookNo`, `bookTitle`, `bookAuthor`, `bookYear`, `bookPrice`, `bookPublisher`) VALUES ('B002', '데이터베이스', '이몽룡', '2020', '25000', '멀티출판사');
INSERT INTO `examDB`.`book` (`bookNo`, `bookTitle`, `bookAuthor`, `bookYear`, `bookPrice`, `bookPublisher`) VALUES ('B003', 'HTML/CSS', '성춘향', '2021', '18000', '강남출판사');
